# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

Use the [`refcodes-archetype-alt-csv`](/refcodes/refcodes-archetype-alt/src/master/refcodes-archetype-alt-csv) archetype to create a *bare metal* [`CSV`](https://en.wikipedia.org/wiki/Comma-separated_values) ([`CLI`](https://en.wikipedia.org/wiki/Command-line_interface)) driven [Java](https://en.wikipedia.org/wiki/Java_(programming_language)) application:

## Getting started ##

> Please refer to the [refcodes-archetype: Using the REFCODES.ORG toolkit made easy](https://www.metacodes.pro/refcodes/refcodes-archetype) documentation for an up-to-date and detailed description on the usage of this artifact.

## How do I get set up? ##

To get up and running, you use this archetype together with [`Maven`](https://maven.apache.org):

> Please adjust `my.corp` with your actual `Group-ID` and `myapp` with your actual `Artifact-ID`:

```
mvn archetype:generate \
  -DarchetypeGroupId=org.refcodes \
  -DarchetypeArtifactId=refcodes-archetype-alt-csv \
  -DarchetypeVersion=3.3.9 \
  -DgroupId=my.corp \
  -DartifactId=myapp \
  -Dversion=0.0.1
```

Using the defaults, this will generate an application processing [`CSV`](https://en.wikipedia.org/wiki/Comma-separated_values) files by harnessing the [`refcodes-tabular`](https://bitbucket.org/refcodes/refcodes-tabular) library.

> For mor information, see the `README.md` file of your brand new artifact!

## Contribution guidelines ##

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Resources ##

* *[refcodes-cli: Parse your args[]](http://www.refcodes.org/refcodes/refcodes-cli)*
* *[org.refcodes:refcodes-cli@Bitbucket](https://bitbucket.org/refcodes/refcodes-cli)*

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.